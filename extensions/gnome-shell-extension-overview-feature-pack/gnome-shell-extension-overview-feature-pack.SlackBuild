#!/bin/sh

# Slackware build script for gnome-shell-extension-overview-feature-pack 

# Copyright 2022 Frank Honolka <slackernetuk@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM=gnome-shell-extension-overview-feature-pack
SRCNAM=overview-feature-pack
VERSION=${VERSION:-f39c9ed}
BUILD=${BUILD:-1}
TAG=${TAG:-_snuk}
GITSRC=https://github.com/G-dH/overview-feature-pack.git
_commit=f39c9edf2c3fe47461620a1b45fde78bb7e7f7a7


CWD=$(pwd)
TMP=${TMP:-/tmp/snuk}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

ARCH=noarch

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $SRCNAM
git clone $GITSRC || exit 1
cd $SRCNAM
git checkout $_commit || exit 1
make zip 
uuid=`jq -r '.uuid' metadata.json`
mkdir -p $PKG/usr/share/gnome-shell/extensions/"$uuid"
DATA="extension.js metadata.json overviewFeaturePack.js prefs.js settings.js util.js windowSearchProvider.js" || exit 1
cp -a $DATA $PKG/usr/share/gnome-shell/extensions/$uuid

install -Dm644 -t "${PKG}/usr/share/glib-2.0/schemas" \
      schemas/org.gnome.shell.extensions.overview-feature-pack.gschema.xml

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
DOCS="LICENSE README.md"
cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}
