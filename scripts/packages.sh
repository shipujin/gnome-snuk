#!/bin/sh

# Copyright 2012  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Copyright 2013 Chess Griffin <chess.griffin@gmail.com> Raleigh, NC
# Copyright 2013-2018 Willy Sudiarto Raharjo <willysr@slackware-id.org>
# All rights reserved.
#
# Based on the xfce-build-all.sh script by Patrick J. Volkerding
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  --------------------------------------------------------------------------
#  MODIFIED BY FRANK HONOLKA <slackernetuk@gmail.com>
#  --------------------------------------------------------------------------
#

SNUKROOT=$(pwd)


# Check for duplicate sources (default: OFF)
CHECKDUPLICATE=0

src=(
SDL2
cantarell-fonts
gsettings-desktop-schemas
bash-completion
gnome-keyring
pango
gtk4
libnma-gtk4
mozjs91
polkit
gjs
upower
amtk
rest
totem-pl-parser
yelp-xsl
geocode-glib
gnome-autoar
bubblewrap
gnome-desktop
gnome-menus
gnome-video-effects
libwpe
wpebackend-fdo
libdaemon
avahi
geoclue2
xdg-dbus-proxy
webkit2gtk
gnome-online-accounts
grilo
cogl
clutter
clutter-gtk
libchamplain
libgdata
libgweather
libpeas
python-toml
typogrify
smartypants
libgweather4
evolution-data-server
telepathy-glib
gfbgraph
libstemmer
tracker2
tracker3
exempi
libgrss
libgxps
libiptcdata
osinfo-db-tools
osinfo-db
libosinfo
tracker-miners2
tracker-miners3
gsound
gnome-backgrounds
libhandy
libportal
nautilus
zenity
libadwaita
gnome-bluetooth
libgusb
colord
gnome-settings-daemon
colord-gtk
clutter-gtk
clutter-gst
cheese
ostree
libxmlb
appstream-glib
AppStream
flatpak
malcontent
libgnomekbd
gnome-control-center
mutter
gnome-shell
gnome-shell-extensions
gnome-session
gdm
gnome-user-docs
yelp
baobab
brasero
gi-docgen
eog
gspell
seahorse
ytnef
lua
lua52
lua53
luasocket
luajit
highlight
libpst
cmark
evolution
file-roller
gtksourceview5
gnome-calculator
gnome-color-manager
gnome-disk-utility
folks
gnome-maps
lxml
yelp-tools
gnome-nettool
gnome-power-manager
gnome-system-monitor
gnome-tweaks
gnome-weather
gtk-vnc
gnome-common
pcsc-lite
libcacard
spice-protocol
spice
spice-gtk
vinagre
jq
p7zip
chrome-gnome-shell
gtksourceview4
libgit2
libgit2-glib
libdazzle
gitg
libwnck4
jsonrpc-glib
template-glib
gnome-calendar
gnome-characters
xdg-desktop-portal-gtk
xdg-desktop-portal-gnome
gnome-initial-setup
liblouis
orca
blocaled
vte-gtk4
gnome-terminal
gnome-console
gnome-text-editor
debugedit
flatpak-builder
devhelp
sysprof
sphinx_rtd_theme
gnome-builder
python3-dbusmock
umockdev
power-profiles-daemon
yajl
libvirt
libvirt-python
libvirt-glib
liburcu
glusterfs
usbredir
acpica
gtest
snappy
virglrenderer
device-tree-compiler
libnfs
vde2
libiscsi
qemu
libovf-glib
gtk-frdp
gnome-boxes
evince
libgnome-games-support
gnome-mines
retro-gtk
libmanette
gnome-games
five-or-more 
qqwing 
gnome-sudoku
epiphany
libmediaart
libdmapsharing3
gom
liboauth
grilo-plugins
gnome-music
totem
gnome-photos
ffnvcodec-headers
gnome-remote-desktop
gnome-clocks
gnome-font-viewer
psutil
click
python3-wheel
auto-cpufreq
f36-backgrounds
gnome-shell-extension-appindicator
)

for dir in ${src[@]}; do

        # get package name
        package=$(echo $dir)

        # Change to package directory
        cd $SNUKROOT/sources/$dir || exit 1

        # Get the version
        version=$(cat ${package}.SlackBuild | grep "VERSION:" | head -n1 | cut -d "-" -f2 | rev | cut -c 2- | rev)

        # Get the build
        build=$(cat ${package}.SlackBuild | grep "BUILD:" | cut -d "-" -f2 | rev | cut -c 2- | rev)

        echo ${package}-${version}-${build} >> $SNUKROOT/PACKAGES.TXT || exit 1

        # back to original directory
        cd $SNUKROOT
done
